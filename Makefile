.PHONY: migrate makemigrations createsuperuser build

migrate:
	docker-compose -f docker-compose.dev.yaml run --rm django python manage.py migrate

makemigrations:
	docker-compose -f docker-compose.dev.yaml run --rm django python manage.py makemigrations

createsuperuser:
	docker-compose -f docker-compose.dev.yaml run --rm django python manage.py migrate

build:
	docker-compose -f docker-compose.dev.yaml build
