import logging

from django.shortcuts import get_object_or_404
from django.http import JsonResponse
from django.utils import timezone
from rest_framework import serializers
from rest_framework import viewsets
from rest_framework.throttling import UserRateThrottle
from rest_framework.response import Response
from rest_framework.request import Request
from rest_framework.settings import api_settings
from rest_framework_csv.renderers import CSVRenderer
from rest_framework.decorators import action

from django_filters.rest_framework import DjangoFilterBackend
from django_filters import rest_framework as filters

from messages.models import Message
from messages.tasks import send_message


logger = logging.getLogger(__name__)


class MessageFilter(filters.FilterSet):
    min_created_at = filters.DateTimeFilter(
        field_name="created_at", lookup_expr="gte"
    )
    max_created_at = filters.DateTimeFilter(
        field_name="created_at", lookup_expr="lte"
    )
    is_read = filters.BooleanFilter(
        "is_read", lookup_expr="isnull", exclude=True
    )

    class Meta:
        model = Message
        fields = (
            "min_created_at",
            "max_created_at",
            "is_read",
        )


class PerUserMessageCreateThrottle(UserRateThrottle):
    scope = "user-message-create"

    def allow_request(self, request, view):
        if request.method == "POST":
            return super().allow_request(request, view)
        return True


class MessagesCsvRenderer(CSVRenderer):
    header = ["id", "title", "body", "is_sent", "is_read", "created_at"]


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = [
            "id",
            "title",
            "body",
            "is_sent",
            "is_read",
            "created_at",
            "updated_at",
        ]


class MessageViewSet(viewsets.ModelViewSet):
    throttle_classes = [PerUserMessageCreateThrottle]
    renderer_classes = tuple(api_settings.DEFAULT_RENDERER_CLASSES) + (
        MessagesCsvRenderer,
    )
    serializer_class = MessageSerializer
    queryset = Message.objects.all().order_by("-created_at")
    filter_backends = [DjangoFilterBackend]
    filterset_class = MessageFilter

    def list(self, request: Request) -> Response:
        qs = self.filter_queryset(self.get_queryset())
        serializer = MessageSerializer(qs, many=True)
        return Response(serializer.data)

    def create(self, request: Request) -> Response:
        serializer = MessageSerializer(data=request.data)
        if not serializer.is_valid():
            return JsonResponse(serializer.errors, status=400)

        message = serializer.save()
        try:
            send_message.delay(message_id=message.pk)
        except send_message.OperationalError as exc:
            # TODO: запускать задачу в бэкграунде, которая будет выгребать
            #       из БД неотправленные сообщения и переотправлять их
            logger.exception("Sending task raised: %r", exc)

        return JsonResponse(serializer.data, status=201)

    def retrieve(self, request: Request, pk: int = None) -> Response:
        message = get_object_or_404(self.get_queryset(), pk=pk)
        return Response(MessageSerializer(message).data)

    def update(self, request: Request, pk: int = None) -> Response:
        message = get_object_or_404(self.get_queryset(), pk=pk)
        serializer = MessageSerializer(message, data=request.data)
        if not serializer.is_valid():
            return JsonResponse(serializer.errors, status=400)

        message = serializer.save(updated_at=timezone.now())
        return Response(MessageSerializer(message).data)

    def destroy(self, request: Request, pk: int = None) -> Response:
        message = get_object_or_404(self.get_queryset(), pk=pk)
        message.delete()
        return Response(status=200)

    @action(detail=True, methods=["post"])
    def mark_as_read(self, request, pk=None):
        message = self.get_object()
        message.is_read = timezone.now()
        message.save()

        return Response(status=200)
