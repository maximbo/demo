from django.db import models


class MessageQuerySet(models.QuerySet):
    def sent(self):
        return self.filter(is_sent__isnull=False)

    def not_sent(self):
        return self.filter(is_sent__isnull=True)


class Message(models.Model):
    title = models.CharField("Title", max_length=128)
    body = models.TextField("Body", default="")

    is_read = models.DateTimeField(
        "Is read",
        null=True,
        editable=False,
        db_index=True,
    )

    is_sent = models.DateTimeField(
        "Is sent",
        null=True,
        editable=False,
        db_index=True,
    )

    created_at = models.DateTimeField(
        "Created at",
        auto_now_add=True,
        db_index=True,
    )
    updated_at = models.DateTimeField(
        "Updated at",
        null=True,
        editable=False,
    )

    objects = MessageQuerySet.as_manager()

    def __str__(self):
        return self.title
