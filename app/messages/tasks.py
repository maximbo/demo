import logging
from celery import shared_task

from services.message_sender import MessageSender


logger = logging.getLogger(__name__)


@shared_task
def send_message(message_id: int):
    sender = MessageSender()

    try:
        sender.send_message(message_id)
    except sender.SendFailed as exc:
        logger.exception("Message send failed: %r", exc)
        send_message.retry()
