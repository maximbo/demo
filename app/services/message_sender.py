from django.utils import timezone
from messages.models import Message


class MessageSender:
    class SendFailed(Exception):
        ...

    def send_message(self, message_id: int) -> bool:
        matched_rows = Message.objects.filter(
            pk=message_id, is_sent__isnull=True
        ).update(is_sent=timezone.now())

        if not matched_rows:
            raise self.SendFailed
